@extends('layouts.app')

@section('title')

    <title>Edit Link</title>

@endsection

@section('content')

    <ol class='breadcrumb'>
        <li><a href='{{ url('/') }}'>Home</a></li>
        <li><a href='{{ url('/') }}/link'>Link</a></li>
        <li><a href='{{ url('/') }}/link/{{$link->id}}'>{{$link->name}}</a></li>
        <li class='active'>Edit</li>
    </ol>

    <h1>Edit a Link</h1>

    <hr/>
    <div id="editlink"></div>

    <form class="form" role="form" method="POST" action="{{ url('/link/'. $link->id) }}">

        {{ method_field('PATCH') }}

        {{ csrf_field() }}

    <!-- widget_name Form Input -->
        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
            <label class="control-label">Link Name</label>

            <input type="text" class="form-control" name="name" value="{{ $link->name }}">

            @if ($errors->has('name'))
                <span class="help-block">
                    <strong>{{ $errors->first('name') }}</strong>
                </span>
            @endif
        </div>

        <div class="form-group{{ $errors->has('url') ? ' has-error' : '' }}">
            <label class="control-label">Link URL</label>

            <input type="text" class="form-control" name="url" value="{{ $link->url }}">

            @if ($errors->has('url'))
                <span class="help-block">
                    <strong>{{ $errors->first('url') }}</strong>
                </span>
            @endif

        </div>

        <div class="form-group{{ $errors->has('desc') ? ' has-error' : '' }}">
            <label class="control-label">Link Description</label>

            <input type="text" class="form-control" name="desc" value="{{ $link->desc }}">

            @if ($errors->has('desc'))
                <span class="help-block">
                    <strong>{{ $errors->first('desc') }}</strong>
                </span>
            @endif

        </div>

        <div class="form-group">
            <button type="submit" class="btn btn-primary btn-lg">
                Edit
            </button>
        </div>

    </form>


@endsection
