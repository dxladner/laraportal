@extends('layouts.app')

@section('title')

    <title>Create a Link</title>

@endsection

@section('content')

    <ol class='breadcrumb'>
        <li><a href='{{ url('/') }}'>Home</a></li>
        <li><a href='{{ url('/') }}/link'>Link</a></li>
        <li class='active'>Create</li>
    </ol>

    <h2>Create a New Link</h2>

    <hr/>

    {{-- <form class="form" role="form" method="POST" action="{{ url('/link') }}">

    {{ csrf_field() }}

        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">

            <label class="control-label">Link Name</label>

            <input type="text" class="form-control" name="name" value="{{ old('name') }}">

            @if ($errors->has('name'))

                <span class="help-block">
                <strong>{{ $errors->first('name') }}</strong>
                </span>

            @endif

        </div>

        <div class="form-group{{ $errors->has('url') ? ' has-error' : '' }}">

            <label class="control-label">Link URL</label>

            <input type="text" class="form-control" name="url" value="{{ old('url') }}">

            @if ($errors->has('url'))

                <span class="help-block">
                <strong>{{ $errors->first('url') }}</strong>
                </span>

            @endif

        </div>

        <div class="form-group{{ $errors->has('desc') ? ' has-error' : '' }}">

            <label class="control-label">Link Description</label>

            <input type="text" class="form-control" name="desc" value="{{ old('desc') }}">

            @if ($errors->has('desc'))

                <span class="help-block">
                <strong>{{ $errors->first('desc') }}</strong>
                </span>

            @endif

        </div>

        <div class="form-group">

            <button type="submit" class="btn btn-primary btn-lg">

                Create

            </button>

        </div> --}}
        <div id="addlink"></div>
    {{-- </form> --}}

@endsection
