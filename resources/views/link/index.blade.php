@extends('layouts.app')

@section('title')

    <title>Links</title>

@endsection

@section('content')

    <ol class='breadcrumb'>
        <li><a href='{{ url('/') }}'>Home</a></li>
        <li><a href='{{ url('/') }}/link'>Links</a></li>
        <li class='active'>Create</li>
    </ol>

    <h2>Links</h2>

    <hr/>

    @if($links->count() > 0)

        <table class="table table-hover table-bordered table-striped">

         <thead>
         <th>Id</th>
         <th>Name</th>
         <th>URL</th>
         <th>Description</th>
         <th>Date Created</th>
         </thead>

            <tbody>

            @foreach($links as $link)

                <tr>
                    <td>{{ $link->id }}</td>
                    <td><a href="{{ url('/') }}/link/{{ $link->id }}">{{ $link->name }}</a></td>

                    <td>{{ $link->url }}</td>
                    <td>{{ $link->desc }}</td>
                    <td>{{ $link->created_at }}</td>
                </tr>

                @endforeach

            </tbody>

        </table>


    @else

    Sorry, no Links

    @endif
    <div> <a href="{{ url('/') }}/link/create">
    <button type="button" class="btn btn-lg btn-primary">
    Create New Link
    </button></a>
    </div>
@endsection
