@extends('layouts.app')
@section('title')
  <title>Test Page</title>
@endsection
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">TEST</div>

                <div class="panel-body">
                  <h1>This is My Test Page</h1>
                  @if(count($Beatles) > 0)
                    @foreach($Beatles as $Beatle)
                      {{ $Beatle }} <br>
                    @endforeach
                  @else
                    <h1> Sorry, nothing to show… </h1>
                  @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
