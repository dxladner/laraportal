<!DOCTYPE html>
<html lang="en">
<head>
  @include('layouts.meta')
  @yield('title')

   @include('layouts.css')

   @yield('css')
</head>
<body>
    @include('layouts.navmenu')

    <div id="app" class="container">

        @yield('content')

    </div>
    @include('layouts.footer')
    <!-- Scripts -->
    @include('layouts.js')
    @include('Alerts::show')
    @yield('scripts')
</body>
</html>
