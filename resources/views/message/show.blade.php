@extends('layouts.app')

@section('title')

    <title>{{ $message->name }} Message</title>

@endsection

@section('content')

  <ol class='breadcrumb'>
      <li><a href='{{ url('/') }}'>Home</a></li>
      <li><a href='{{ url('/') }}/message'>Message</a></li>
      <li><a href='{{ url('/') }}/message/{{ $message->id }}'>{{ $message->name }}</a></li>
  </ol>

  <h1>{{ $message->name }}</h1>

  <hr/>

  <div class="panel panel-default">

      <!-- Table -->
      <table class="table table-striped">

          <thead>
          <tr>

              <th>Id</th>
              <th>Name</th>
              <th>Message</th>
              <th>Date Created</th>
              <th>Edit</th>
              <th>Delete</th>

          </tr>
          </thead>
          <tbody>

          <tr>
              <td>{{ $message->id }} </td>
              <td> <a href="{{ url('/') }}/message/{{ $message->id }}/edit">
                      {{ $message->name }}</a></td>

              <td>{{ $message->message_text }}</td>

              <td>{{ $message->created_at }}</td>

              <td> <a href="{{ url('/') }}/message/{{ $message->id }}/edit">

                      <button type="button" class="btn btn-default">Edit</button></a></td>

              <td>

                  <div class="form-group">

                      <form class="form" role="form" method="POST" action="{{ url('/message/'. $message->id) }}">
                          <input type="hidden" name="_method" value="delete">
                          {{ csrf_field() }}

                          <input class="btn btn-danger" Onclick="return ConfirmDelete();" type="submit" value="Delete">

                      </form>
                  </div>
              </td>

          </tr>
          </tbody>

      </table>
  </div>
@endsection
@section('scripts')
    <script>
        function ConfirmDelete()
        {
            var x = confirm("Are you sure you want to delete?");
            if (x){
                return true;
            } else {
                return false;
            }
        }
    </script>
@endsection
