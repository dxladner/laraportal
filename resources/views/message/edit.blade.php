@extends('layouts.app')

@section('title')

    <title>Edit Message</title>

@endsection

@section('content')

    <ol class='breadcrumb'>
        <li><a href='{{ url('/') }}'>Home</a></li>
        <li><a href='{{ url('/') }}/message'>Message</a></li>
        <li><a href='{{ url('/') }}/message/{{$message->id}}'>{{$message->name}}</a></li>
        <li class='active'>Edit</li>
    </ol>

    <h1>Edit Message</h1>

    <hr/>


    <form class="form" role="form" method="POST" action="{{ url('/message/'. $message->id) }}">

        {{ method_field('PATCH') }}

        {{ csrf_field() }}

    <!-- widget_name Form Input -->
        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
            <label class="control-label">Message Name</label>

            <input type="text" class="form-control" name="name" value="{{ $message->name }}">

            @if ($errors->has('name'))
                <span class="help-block">
                    <strong>{{ $errors->first('name') }}</strong>
                </span>
            @endif
        </div>

        <div class="form-group{{ $errors->has('message_text') ? ' has-error' : '' }}">
            <label class="control-label">Message</label>

            <input type="text" class="form-control" name="message_text" value="{{ $message->message_text }}">

            @if ($errors->has('message_text'))
                <span class="help-block">
                    <strong>{{ $errors->first('message_text') }}</strong>
                </span>
            @endif

        </div>

        <div class="form-group">
            <button type="submit" class="btn btn-primary btn-lg">
                Edit
            </button>
        </div>

    </form>

@endsection
