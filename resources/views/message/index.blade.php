@extends('layouts.app')

@section('title')

    <title>Messages</title>

@endsection

@section('content')

    <ol class='breadcrumb'>
        <li><a href='{{ url('/') }}'>Home</a></li>
        <li><a href='{{ url('/') }}/message'>Messages</a></li>
        <li class='active'>Create</li>
    </ol>

    <h2>Messages</h2>

    <hr/>

    @if($messages->count() > 0)

        <table class="table table-hover table-bordered table-striped">

         <thead>
         <th>Id</th>
         <th>Name</th>
         <th>Message</th>
         <th>Date Created</th>
         </thead>

            <tbody>

            @foreach($messages as $message)

                <tr>
                    <td>{{ $message->id }}</td>
                    <td><a href="{{ url('/') }}/message/{{ $message->id }}">{{ $message->name }}</a></td>

                    <td>{{ $message->message_text }}</td>
                    <td>{{ $message->created_at }}</td>
                </tr>

                @endforeach

            </tbody>

        </table>


    @else

    Sorry, no Messages

    @endif
    <div> <a href="{{ url('/') }}/message/create">
    <button type="button" class="btn btn-lg btn-primary">
    Create New
    </button></a>
    </div>
@endsection
