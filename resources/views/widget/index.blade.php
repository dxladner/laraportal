@extends('layouts.app')

@section('title')

    <title>Widgets</title>

@endsection

@section('content')

    <ol class='breadcrumb'>
        <li><a href='{{ url('/') }}'>Home</a></li>
        <li><a href='{{ url('/') }}/widget'>Widgets</a></li>
        <li class='active'>Create</li>
    </ol>

    <h2>Widgets</h2>

    <hr/>

    @if($widgets->count() > 0)

        <table class="table table-hover table-bordered table-striped">

         <thead>
         <th>Id</th>
         <th>Name</th>
         <th>Date Created</th>
         </thead>

            <tbody>

            @foreach($widgets as $widget)

                <tr>
                    <td>{{ $widget->id }}</td>
                    <td><a href="{{ url('/') }}/widget/{{ $widget->id }}-{{ $widget->slug }}">{{ $widget->name }}</a></td>
                    <td>{{ $widget->created_at }}</td>
                </tr>

                @endforeach

            </tbody>

        </table>


    @else

    Sorry, no Widgets

    @endif
    <div> <a href="{{ url('/') }}/widget/create">
    <button type="button" class="btn btn-lg btn-primary">
    Create New
    </button></a>
    </div>
@endsection
