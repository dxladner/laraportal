@extends('layouts.app')
@section('title')
  <title>Home</title>
@endsection
@section('content')
<div class="container">
    <div class="row">

        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">Users</div>
                <div class="panel-body">
                    <div id="userlist"></div>
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">Messages</div>
                <div class="panel-body">
                    <div id="messagelist"></div>
                </div>
            </div>
        </div>

    </div>

    <div class="row">

        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Seach Employees</div>
                <div class="panel-body">
                    <div id="empsearch"></div>
                </div>
            </div>
        </div>

    </div>

    <div class="row">

      <div class="col-md-8">
          <div class="panel panel-default">
              <div class="panel-heading">Links</div>
              <div class="panel-body">
                  <div id="linklist"></div>
              </div>
          </div>
      </div>

      <div class="col-md-4">
          <div class="panel panel-default">
              <div class="panel-heading">Job Links</div>
              <div class="panel-body">
                  <div id="joblist"></div>
              </div>
          </div>
      </div>

    </div>
</div>
@endsection
