import React, { Component } from 'react';
import ReactDOM from 'react-dom';

class Messages extends Component {
    constructor(props) {
        super(props);

        this.state = {
            messages: []
        }
    }

    componentDidMount() {
        fetch('http://localhost/laraportal/public/api/messages')
            .then(response => {
                return response.json();
            })
            .then(messages => {
                this.setState({ messages });
            });
    }

    renderMessages() {
        return this.state.messages.map(message => {
            return (
                <tr key={message.id}>
                    <td>{ message.id }</td>
                    <td>{ message.name }</td>
                    <td>{ message.message_text }</td>
                </tr>
            );
        })
    }

    render() {
        return (
            <div>

                <table className="table">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Message</th>
                        </tr>
                    </thead>

                    <tbody>
                        { this.renderMessages() }
                    </tbody>
                </table>
            </div>
        );
    }
}

export default Messages;

if (document.getElementById('messagelist')) {
    ReactDOM.render(<Messages />, document.getElementById('messagelist'));
}
