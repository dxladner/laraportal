import React, { Component } from 'react';
import ReactDOM from 'react-dom';

class Employees extends Component {
    constructor(props) {
        super(props);

        this.state = {
            employees: [],
            lastName: 'Darren'
        }
    }

    componentWillMount(){
      this.getEmps();
    }

    getEmps() {
          fetch('http://localhost/portal/public/api/user/'+this.state.lastName)
            .then(response => {
                return response.json();
            })
            .then(employees => {
                this.setState({ employees });
            });
    }

    renderEmployees() {
        return this.state.employees.map(employee => {
            return (
                <tr key={ employee.id }>
                    <td>{ employee.id }</td>
                    <td>{ employee.name }</td>
                    <td>{ employee.email }</td>
                </tr>
            );
        })
    }

    onChange(e){
      this.setState({lastName: e.target.value}, this.getEmps());
    }

    render() {
        return (
            <div>
                <input type="text" className="form-control"
                  value={this.state.value}
                  placeholder="Search Employees...."
                  onChange={this.onChange.bind(this)}
                />
                <table className="table">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Email</th>
                        </tr>
                    </thead>

                    <tbody>
                        { this.renderEmployees() }
                    </tbody>
                </table>
            </div>
        );
    }
}

export default Employees;

if (document.getElementById('empsearch')) {
    ReactDOM.render(<Employees />, document.getElementById('empsearch'));
}
