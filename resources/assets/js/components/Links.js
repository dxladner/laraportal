import React, { Component } from 'react';
import ReactDOM from 'react-dom';

class Links extends Component {
    constructor(props) {
        super(props);

        this.state = {
            links: []
        }
    }

    componentDidMount() {
        fetch('http://localhost/laraportal/public/api/links')
            .then(response => {
                return response.json();
            })
            .then(links => {
                this.setState({ links });
            });
    }

    renderLinks() {
        return this.state.links.map(link => {
            return (
                <tr key={link.id}>
                    <td>{ link.id }</td>
                    <td>{ link.name }</td>
                    <td>{ link.url }</td>
                    <td>{ link.desc }</td>
                </tr>
            );
        })
    }

    render() {
        return (
            <div>

                <table className="table">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>URL</th>
                            <th>Desc</th>
                        </tr>
                    </thead>

                    <tbody>
                        { this.renderLinks() }
                    </tbody>
                </table>
            </div>
        );
    }
}

export default Links;

if (document.getElementById('linklist')) {
    ReactDOM.render(<Links />, document.getElementById('linklist'));
}
