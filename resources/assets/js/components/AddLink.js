import React, { Component } from 'react';
import ReactDOM from 'react-dom';

class AddLink extends Component {
      constructor(props){
        super(props);
        this.state = {
          name:'',
          url:'',
          desc: '',
          message: ''
        }
        this.handleFormSubmit = this.handleFormSubmit.bind(this);
    }

    handleFormChange(event){
      var name = this.refs.name.value;
      var url = this.refs.url.value;
      var desc = this.refs.desc.value;

      this.setState({name:'', url: '', desc: ''}, function(){
      //  console.log(this.state);
      });
    }

    handleFormSubmit(event){
      var name = this.refs.name.value;
      var url = this.refs.url.value;
      var desc = this.refs.desc.value;

      this.setState({name:name, url: url, desc: desc}, function(){
      //  console.log(this.state);
      });
      fetch('http://localhost/laraportal/public/api/links?name='+name+'&url='+url+'&desc='+desc,
      {
        method: "POST"
      }).then( (response) => {
          return response.json()
      }).then( (json) => {
          this.setState({message: json.message});
      }).catch((error) => {
          //this.setState({message: error});
          console.log(error);
      });
      this.refs.addLinkForm.reset();
      var newMessage = '';
      setTimeout(function(self) {
        self.setState({message: newMessage});
      }, 4000, this);
      event.preventDefault();
    }

    render() {

        return (
            <div>
            <div className="success-message" ref="formMessage">{this.state.message}</div>
            <form ref="addLinkForm" onSubmit={this.handleFormSubmit.bind(this)}>
                <div className="form-group">
                  <label htmlFor="name">Link Name</label>
                  <input type="text" className="form-control" id="name" name="name" onChange={this.handleFormChange.bind(this)}  ref="name" />
                </div>
                <div className="form-group">
                  <label htmlFor="url">Link URL</label>
                  <input type="text" className="form-control" id="url" name="url" onChange={this.handleFormChange.bind(this)} ref="url" />
                </div>
                <div className="form-group">
                  <label htmlFor="desc">Link Description</label>
                  <input type="text" className="form-control" id="desc" name="desc" onChange={this.handleFormChange.bind(this)} ref="desc" />
                </div>
                <button type="submit" className="btn btn-primary btn-lg">Create Link</button>&nbsp;&nbsp;
                <a href="http://localhost/laraportal/public/link" className="btn btn-default btn-lg">Links List</a>
              </form>
            </div>
        );
    }
}

export default AddLink;

if (document.getElementById('addlink')) {
    ReactDOM.render(<AddLink />, document.getElementById('addlink'));
}
