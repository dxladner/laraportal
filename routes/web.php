<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(array('prefix' => 'api/'), function()
{
  header('Access-Control-Allow_Headers: Origin, X-Requested-With, Content-Type, Accept');

  Route::resource('users', 'UsersapiController');
  Route::resource('messages', 'MessagesapiController');
  Route::resource('links', 'LinksapiController');

  Route::get('/user/{name}', 'UsersapiController@searchByLastName');

});



Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index');

Route::get('test', 'TestController@index');

/** Link **/
Route::resource('link', 'LinksController');

/** Message **/
Route::resource('message', 'MessageController');

/** Widgets **/
Route::get('widget/create', ['as' => 'widget.create', 'uses' => 'WidgetController@create']);
Route::get( 'widget/{id}-{slug?}', ['as' => 'widget.show', 'uses' => 'WidgetController@show']);
Route::resource('widget', 'WidgetController', ['except' => ['show', 'create']]);
