<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Link;
use Auth;
use DB;
use Response;
class LinksapiController extends Controller
{
    public function index()
    {
      $links = Link::all();
      return $links;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
     public function store(Request $request)
     {
        $link = new Link($request->all());
        $link->save();

        return Response::json([
          'message'	=> 'Link successfully created.'
        ], 200);
     }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        return Link::find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        $link = Link::find($id);
		    $link->fill($request->only(['name', 'url', 'desc']));
        $link->save();

        return $link;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $link = Link::find($id);
        $link->delete();

        return \Response::json(['success' => true]);
    }
}
