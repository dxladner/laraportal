<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Link;
use Redirect;

class LinksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $links = Link::all();
      	return view('link.index', compact('links'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('link.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $this->validate($request, [

	        'name' 	=> 'required|unique:widgets|string|max:30',
	        'url' 	=> 'required',
	        'desc' 	=> '',

	      ]);

	      $link = Link::create(['name' 	=> $request->name,
	                            'url' 	=> $request->url,
	                            'desc' 	=> $request->desc,    
	                        	]);

	      $link->save();

	      alert()->success('Congrats!', 'You made a Link');

	      return Redirect::route('link.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $link = Link::findOrFail($id);
       	return view('link.show', compact('link'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $link = Link::findOrFail($id);
    	return view('link.edit', compact('link'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [

	      'name'	=> 'required|string|max:30',
	      'url' 	=> 'required',
	      'desc' 	=> '',

	    ]);

	    $link = Link::findOrFail($id);

	    $link->update(['name' 	=> $request->name,
	                    'url'	=> $request->url,
	                    'desc' 	=> $request->desc,
	                	]);

	    $link->save();

	    alert()->success('Congrats!', 'You updated a link');

	    return Redirect::route('link.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Link::destroy($id);

    	alert()->overlay('Attention!', 'You deleted a link', 'error');

    	return Redirect::route('link.index');
    }
}
