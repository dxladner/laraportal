<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Message;
use Redirect;

class MessageController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
      $messages = Message::all();

      return view('message.index', compact('messages'));
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
      return view('message.create');
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
      $this->validate($request, [

        'name' => 'required|string|max:30',
        'message_text' => 'required',

      ]);

      $message = Message::create(['name' 		=> $request->name,
                                'message_text' 	=> $request->message_text,
                                ]);

      $message->save();

      alert()->success('Congrats!', 'You made a Message');

      return Redirect::route('message.index');
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
   public function show($id)
   {
       $message = Message::findOrFail($id);
       return view('message.show', compact('message'));
   }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    $message = Message::findOrFail($id);
    return view('message.edit', compact('message'));
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    $this->validate($request, [
      'name' 			=> 'required|string|max:30',
      'message_text' 	=> 'required',
    ]);

    $message = Message::findOrFail($id);

    $message->update(['name' 			=> $request->name,
                      'message_text' 	=> $request->message_text,
                      ]);

    $message->save();

    alert()->success('Congrats!', 'You updated a message');

    return Redirect::route('message.index');
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    Message::destroy($id);

    alert()->overlay('Attention!', 'You deleted a message', 'error');

    return Redirect::route('message.index');
  }
}
