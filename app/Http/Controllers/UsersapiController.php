<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Response;
use DB;


class UsersapiController extends Controller{

  public function index()
  {
    $users = User::all();
    return $users;
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function searchByLastName($name)
  {
      $name = $name.'%';
      $result = DB::table('users')
            ->select(DB::raw("*"))
            ->where('name', 'like', $name)
            ->get();
      return $result;
  }
}
